# ee-ansible-ssa

[ansible-builder](https://github.com/ansible/ansible-builder) configuration for building an Execution Environment for the [Ansible SSA](https://www.ansible-labs.de) project.

## Code only

This project contains the execution environment definition file, but does not provide the actual container image. This is due to the requirement of having an active Red Hat Ansible Automation Platform subscription to be able to download some of the Collections used and the base [ee-supported-rhel8](https://catalog.redhat.com/software/containers/ansible-automation-platform-22/ee-supported-rhel8/6177cff2be25a74c009224fa) container image.

If you are a Red Hat Employee, you can download the Execution Environment from [Quay](quay.io) by using the [ee-ansible-ssa](https://quay.io/repository/redhat_emp1/ee-ansible-ssa) repository. You will need the Employee flag in your Quay account, to get access to the repository.

```bash
# log in with your Red Hat credentials
podman login quay.io
# pull down the execution environment
podman pull quay.io/redhat_emp1/ee-ansible-ssa
```

## Requirements

To build the container image [ansible-builder](https://github.com/ansible/ansible-builder) has to be installed. It can be installed with `pip install ansible-builder` or retrieved from Red Hat and [installed via the Software Channel](https://docs.redhat.com/en/documentation/red_hat_ansible_automation_platform/2.4/html/creating_and_consuming_execution_environments/assembly-using-builder).

If you want to use your own instance of private automation hub, use the provided [requirements-galaxy.yml](./requirements-galaxy.yml) and [requirements-hub.yml](./requirements-hub.yml) to synchronize the required collections.

## Collection dependencies

To successfully build this execution environment a number of collections have to be available on automation hub. Sync Ansible Galaxy content with the provided [requirements-galaxy.yml](requirements-galaxy.yml) file (import it in the Repository Management dialog of the automation hub UI).

Also synchronize the certified and validated content collections listed in [requirements-hub.yml](requirements-hub.yml) from Red Hat Automation Hub. Note that validated and certified content are two different repositories and have to be configured separately.

Check the [requirements.yml](./ee-ansible-ssa/requirements.yml) for specific versions and additional details.

## Build image

```bash
# log into the Red Hat registry to be able to pull the ee-supported-rhel8
podman login registry.redhat.io
cd ee-ansible-ssa
# set tag to the proper version, e.g
tag=0.4.11
ansible-builder build -f ee-ansible-ssa.yml -t ee-ansible-ssa:$tag
# you might want to add -v 3 to get more details
ansible-builder build -f ee-ansible-ssa.yml -t ee-ansible-ssa:$tag -v 3
```

## Push the image

```bash
podman login quay.io
podman push ee-ansible-ssa:$tag quay.io/redhat_emp1/ee-ansible-ssa:$tag
podman push ee-ansible-ssa:$tag quay.io/redhat_emp1/ee-ansible-ssa:latest
```
